import java.util.*;
import java.util.stream.Collectors;

public class WordFrequencyGame {

    public static final String SPACE_BREAK = "\\s+";
    public static final String LINE_BREAK = "\n";
    public static final int INIT_COUNT = 1;

    public String getResult(String inputStr){

        try {
            List<Input> inputList = transformToInputs(inputStr);
            Map<String, Integer> wordFrequencyMap = transformToMap(inputList);
            List<Input> wordFrequencyList = getWordFrequencyList(wordFrequencyMap);
            sortedByFrequencyInDescending(wordFrequencyList);
            return getWordFrequencyListString(wordFrequencyList);
        } catch (Exception e) {
            return "Calculate Error";
        }
    }

    private static List<Input> transformToInputs(String sentence) {
        String[] splitWords = sentence.split(SPACE_BREAK);
        return Arrays.stream(splitWords)
                .map(word -> new Input(word, INIT_COUNT))
                .collect(Collectors.toList());
    }

    private Map<String, Integer> transformToMap(List<Input> wordFrequencyList) {
        Map<String, Integer> map = new HashMap<>();
        for (Input wordFrequency : wordFrequencyList) {
            if (!map.containsKey(wordFrequency.getValue())) {
                map.put(wordFrequency.getValue(), INIT_COUNT);
            } else {
                map.put(wordFrequency.getValue(), wordFrequency.getWordCount() + 1);
            }
        }
        return map;
    }

    private static List<Input> getWordFrequencyList(Map<String, Integer> wordFrequencyMap) {
        List<Input> list = new ArrayList<>();
        for (Map.Entry<String, Integer> entry : wordFrequencyMap.entrySet()){
            list.add(new Input(entry.getKey(), entry.getValue()));
        }
        return list;
    }

    private static void sortedByFrequencyInDescending(List<Input> wordFrequencyList) {
        wordFrequencyList.sort((word1, word2) -> word2.getWordCount() - word1.getWordCount());
    }

    private static String getWordFrequencyListString(List<Input> wordFrequencyList) {
        StringJoiner joiner = new StringJoiner(LINE_BREAK);
        for (Input word : wordFrequencyList) {
            joiner.add(word.getValue() + " " +word.getWordCount());
        }
        return joiner.toString();
    }

}
